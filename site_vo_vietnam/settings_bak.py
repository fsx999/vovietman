# Django settings for site_vo project.
import os
from django.conf import global_settings
from django.utils.translation import gettext_noop


DEBUG = True
TEMPLATE_DEBUG = DEBUG
gettext = lambda s: s
PROJECT_ROOT = PROJECT_PATH = PROJECT_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir))
ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)
EMAIL_HOST = 'smtp.iedparis8.net'
EMAIL_USE_TLS = True
EMAIL_HOST_USER = 'paul.guichon@iedparis8.net'
EMAIL_HOST_PASSWORD = 'naharisoa$1'
INTERNAL_IPS = ('127.0.0.1',)
MANAGERS = ADMINS
LOCALE_PATHS = (PROJECT_ROOT+"/locale",)
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': PROJECT_DIR + '/site_vo.db',                      # Or path to database file if using sqlite3.
        # The following settings are not used with sqlite3:
        'USER': '',
        'PASSWORD': '',
        'HOST': '',                      # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
        'PORT': '',                      # Set to empty string for default.
    }
}

# Hosts/domain names that are valid for this site; required if DEBUG is False


# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'Europe/Paris'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'fr-fr'
LANGUAGES = (
    ('fr', 'France'),
)
SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/var/www/example.com/media/"

MEDIA_ROOT = os.path.join(PROJECT_PATH, "media")
# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://example.com/media/", "http://media.example.com/"
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/var/www/example.com/static/"

STATIC_ROOT = os.path.join(PROJECT_PATH, "static")
# URL prefix for static files.
# Example: "http://example.com/static/", "http://static.example.com/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = '=a4$ra$bt_(@7+5as8hslbfer&!on_88k0(m@2@2znt-fo&$!h'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS = global_settings.TEMPLATE_CONTEXT_PROCESSORS + (
    'django.core.context_processors.request',
    'zinnia.context_processors.version',
    'cms.context_processors.media',
    'sekizai.context_processors.sekizai',
    'django_messages.context_processors.inbox',
    'djangobb_forum.context_processors.forum_settings',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.cache.UpdateCacheMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.doc.XViewMiddleware',
    'django.middleware.common.CommonMiddleware',
    'cms.middleware.page.CurrentPageMiddleware',
    'cms.middleware.user.CurrentUserMiddleware',
    'cms.middleware.toolbar.ToolbarMiddleware',
    'cms.middleware.language.LanguageCookieMiddleware',
    #'debug_toolbar.middleware.DebugToolbarMiddleware',
    'pagination.middleware.PaginationMiddleware',
    #'django_authopenid.middleware.OpenIDMiddleware',
    # 'django.middleware.cache.FetchFromCacheMiddleware',
    'django.middleware.transaction.TransactionMiddleware',
    'djangobb_forum.middleware.LastLoginMiddleware',
    'djangobb_forum.middleware.UsersOnline',
    'djangobb_forum.middleware.TimezoneMiddleware',
)

ROOT_URLCONF = 'site_vo_vietnam.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'site_vo_vietnam.wsgi.application'

TEMPLATE_DIRS = (os.path.join(os.path.dirname(__file__), '..', 'templates').replace('\\','/'),)

INSTALLED_APPS = (
#'debug_toolbar',
    #'zinnia_bootstrap',
    'SLQT-FIVV',
    # 'admin_tools_zinnia',
    # 'admin_tools',
    # 'admin_tools.theming',
    # 'admin_tools.menu',
    # 'admin_tools.dashboard',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sitemaps',
    'django.contrib.humanize',
    'django.contrib.comments',
    'django_extensions',
    # Uncomment the next line to enable the admin:
    'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
    'base',
    'djangocms_text_ckeditor',
    # 'easy_maps',
    'cms',
    # 'cms_themes',
    'menus',
    # 'zinnia',
    'sekizai',
    'mptt',
    'reversion',
    'tinymce',
    'tagging',
    #'cms.plugins.twitter',
    # 'cmsplugin_pdf',
    # 'filer',
    # 'easy_thumbnails',
    # 'cms.plugins.flash',
    # 'cms.plugins.googlemap',
    # 'cms.plugins.link',
    # 'cmsplugin_filer_file',
    # 'cmsplugin_filer_folder',
    # 'cmsplugin_filer_image',
    # 'cmsplugin_filer_teaser',
    # 'cmsplugin_filer_video',
    # 'cmsplugin_nivoslider', # disporama sur dossier
    #'cmsplugin_filery', # galery image par image
    'south',
    'tagging',
    'twcarousel',  # caroulsel de twiter
    # 'cmsplugin_zinnia',
    'registration',
    'pagination',
    'django_messages',
    #'django_authopenid',
    'djangobb_forum',
    'haystack',


)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}
ADMIN_TOOLS_INDEX_DASHBOARD = "dashboard.CustomIndexDashboard"
ADMIN_TOOLS_MENU = 'menu.CustomMenu'

THUMBNAIL_PROCESSORS = (
    'easy_thumbnails.processors.colorspace',
    'easy_thumbnails.processors.autocrop',
    #'easy_thumbnails.processors.scale_and_crop',
    'filer.thumbnail_processors.scale_and_crop_with_subject_location',
    'easy_thumbnails.processors.filters',
)
CMS_TEMPLATES = (
    ('template_1.html', 'Template with sidebar'),
    ('template_2.html', 'Template simple'),
    ('accueil.html', 'Template de l\'accueil'),
)

LANGUAGES = [
    ('fr', gettext_noop('French')),
]

DEBUG_TOOLBAR_CONFIG = {
    'INTERCEPT_REDIRECTS': False
}
THUMBNAIL_PROCESSORS = (
    'easy_thumbnails.processors.colorspace',
    'easy_thumbnails.processors.autocrop',
    #'easy_thumbnails.processors.scale_and_crop',
    'filer.thumbnail_processors.scale_and_crop_with_subject_location',
    'easy_thumbnails.processors.filters',
)
ZINNIA_AUTO_CLOSE_PINGBACKS_AFTER = 0
ZINNIA_AUTO_CLOSE_TRACKBACKS_AFTER = 0
ZINNIA_PAGINATION = 5
ALLOWED_HOSTS = ['test-vo-vietnam.narsu.net']
CMS_PERMISSION = True
HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.whoosh_backend.WhooshEngine',
        'PATH': os.path.join(PROJECT_ROOT, 'djangobb_index'),
        'INCLUDE_SPELLING': True,
    },
}
HAYSTACK_SIGNAL_PROCESSOR = 'haystack.signals.RealtimeSignalProcessor'
ACCOUNT_ACTIVATION_DAYS = 10
LOGIN_REDIRECT_URL = '/forum/'
LOGIN_URL = '/forum/account/signin/'
try:
    from local_settings import *
except ImportError:
    pass

#description du forum
DJANGOBB_HEADER = 'Accueil'
DJANGOBB_TAGLINE = 'Forum du vo-vietnam'
DJANGOBB_FORUM_BASE_TITLE = 'Accueil'
DJANGOBB_LOFI_SUPPORT = False

