from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from django.views.generic import TemplateView
from registration.backends.default.views import RegistrationView
from django.contrib.auth import views as auth_views
from base.views import SallesView

admin.autodiscover()
from django.conf import settings


urlpatterns = patterns('',
                       # Examples:
                       # url(r'^$', 'site_vo.views.home', name='home'),
                       # url(r'^site_vo/', include('site_vo.foo.urls')),

                       # Uncomment the admin/doc line below to enable admin documentation:
                       url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
                        # url(r'^blog/', include('zinnia.urls')),
                       # Uncomment the next line to enable the admin:
                       #(r'^forum/account/', include('django_authopenid.urls')),
                       url(r'^calendar/', include('calendarium.urls')),
                       (r'^forum/account/', include('registration.backends.default.urls')),
                       url(r'^admin/', include(admin.site.urls)),
                       # url(r'^admin_tools/', include('admin_tools.urls')),
                        url(r'^salles/', include('base.urls')),
                       (r'^forum/', include('djangobb_forum.urls', namespace='djangobb')),
                       url(r'^comments/', include('django.contrib.comments.urls')),
                       (r'^forum/pm/', include('django_messages.urls')),
                       (r'^tinymce/', include('tinymce.urls')),
                       url(r'^password/reset/$', auth_views.password_reset, name='auth_password_reset'),
                       url(r'^password/reset/confirm/(?P<uidb36>[0-9A-Za-z]+)-(?P<token>.+)/$',
                           auth_views.password_reset_confirm,
                           name='auth_password_reset_confirm'),
                       url(r'^password/reset/complete/$',
                           auth_views.password_reset_complete,
                           name='auth_password_reset_complete'),
                       url(r'^password/reset/done/$',
                           auth_views.password_reset_done,
                           name='auth_password_reset_done'),
                       url(r'^password/$', 'django.contrib.auth.views.password_change',
                           {'template_name': 'authopenid/password_change_form.html'}, name='auth_password_change'),
                       (r'^account/', include('django.contrib.auth.urls')),
                       url(r'^signin/$', 'django.contrib.auth.views.login', {'template_name': 'authopenid/signin.html'},
                           name='user_signin'),
                       url(r'^signup/$', RegistrationView.as_view(), name='registration_register', ),
                       url(r'^signup/complete/$',
                           TemplateView.as_view(template_name='registration/registration_complete.html'),
                           name='registration_complete'),
                       url(r'^signup/closed/$',
                           TemplateView.as_view(template_name='registration/registration_closed.html'),
                           name='registration_disallowed'),
                       url(r'^', include('cms.urls')),




)

if settings.DEBUG:
    urlpatterns = patterns(
        '',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
            {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
        url(r'', include('django.contrib.staticfiles.urls')), ) + urlpatterns
