__author__ = 'paul'
from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _
from menu import TestMenu
class BaseApp(CMSApp):
    name = _("My Apphook")
    urls = ["base.urls"]
    menus = [TestMenu]

# apphook_pool.register(BaseApp)
